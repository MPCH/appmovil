﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppE1
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(true)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }


        //Metodo asincrono
        private async void Button_Clicked(object sender, EventArgs e)
        {
                var text1 = Mensaje1.Text;
                var text2 = Mensaje2.Text;
                //await Navigation.PushAsync(new Page1(text1, text2));
                Navigation.InsertPageBefore(new Page1(text1, text2), this);
                await Navigation.PopAsync();
        }

    }
}
